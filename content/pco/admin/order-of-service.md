---
date: 2024-06-23
tags:
    - pco
title: Service Planning
TocOpen: true
---

If you work with planning the order of our morning gatherings, you may be helped
by a few articles:
- start with
  [planning basics](https://pcoservices.zendesk.com/hc/en-us/articles/204461180-Set-Up-the-Order-of-Service)
- be mindful when making late changes (the week of) to a plan, and
  [inform others of your changes](https://pcoservices.zendesk.com/hc/en-us/articles/360025912914-Track-Plan-Changes),
  in particular the music and tech teams.

Additionally, you will be helped with configuring your experience (only the
first time):
- configure the
  [columns shown for items in the order of service](https://pcoservices.zendesk.com/hc/en-us/articles/1260802688470-Plan-Page-Overview#OrderTab)
  - you will want to show "Passage" and "Responsible" notes
- our items are color coded, but you need to
  [enable row colors](https://pcoservices.zendesk.com/hc/en-us/articles/360059012033-Item-Row-Colors#EnableItemRowColors)
  to see them

With that in mind, we will be helped by being consistent. To this end, There is
a "Liturgy" template for each service type, as well as a "Communion" and
"Baptism" template for Sunday Mornings. When you start planning a service, first
[import the template into the plan](https://pcoservices.zendesk.com/hc/en-us/articles/7341381180827-Import-a-Template-or-Plan),
selecting only "Items". This will populate the plan with common items in our
orders of service. (Note: importing a template only adds to the plan, so you may
do it even after having already started adding items; your previous ones are
kept.) (Note 2: you may also import templates in the
[Matrix View](../matrix-view)!)

For each *item* (not song) in our order of service, you have the option of
adding notes for "Passage" (if a particular passage goes with that item) or
"Responsible" (if a particular person is responsible for that item). If the
person responsible for that item is the overall leader of the service (i.e. one
of our elders, if a guest preacher is speaking), leave "Responsible" blank. For
example, "Call to Worship" will have a passage, but will be done by the person
leading the whole service.

*Note: when uploading our previous orders of service, "Passage" was entered into
the item description. I intended to work back through those and correct that.
Please enter this in the "Passage" notes going forward.*

Please name the items as follows, if one of these match (you may have an entirely new item…add it here if it is frequent!):
- Announcements
- Call to Worship
  - including a moment prior to the call to prepare our hearts
- Corporate Reading
  - usually lead by the music leader
- Scripture Reading
  - not corporate; usually followed by a corporate prayer
- Prayer of Confession
- Prayer of Praise
- Pastoral Prayer
- Offering
- Sermon Text Reading
- Sermon
- Testimony
- Baptism
- Benediction

A note when adding songs. You can add a song
[either by a shortcut key or using the "Add" menu](https://pcoservices.zendesk.com/hc/en-us/articles/204461180-Set-Up-the-Order-of-Service).
When using the "Add" menu, you have the added advantage of seeing the last time
the song was used in a service.

## Rescheduling a plan

Planning Center allows you to reschedule, or move, a plan, either the order
of service only, the volunteer schedule only, or the entire plan.

For example, you may decide we need to shift all of the orders of service
back one week, but you don't want to adjust the volunteer assignments.

See [Planning Center's article on rescheduling plans](https://pcoservices.zendesk.com/hc/en-us/articles/360045037494-Rescheduling-Plans).
