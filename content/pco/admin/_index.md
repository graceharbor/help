---
title: Using PCO as a Manager
summary: How to use Planning Center for service planning or volunteer scheduling.
date: 2024-06-23
weight: 12
---

<style type="text/css">
  .add_plan {
    font-weight: bold;
    color: green;
    border: 1px solid green;
    border-radius: 5px;
    padding: 0 3px;
  }
</style>

Our gatherings are planned under three different [Service Types](https://pcoservices.zendesk.com/hc/en-us/sections/200765904-Service-Types):

- Sunday Morning
- Sunday Evening
- Member's Meeting

When you first go to
[services.planningcenteronline.com](https://services.planningcenteronline.com/dashboard/0),
you are shown by default only folders that organize our service plans. Open the
"GHC Gatherings" folder (the next time you visit, it will remember which folder
you were viewing!) and you will see all upcoming services, in the service type
order shown above. If you need to plan services which are not yet created, you
will need to click <span class="add_plan">+ Add plan</span> at the bottom of the
Service Type listing. You may select a template now, or you can import a
template later once editing a plan.

If you plan to plan multiple services at once, I have found the
[Matrix View](./matrix-view) to be especially helpful.
