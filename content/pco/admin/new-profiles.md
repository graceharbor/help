---
date: 2024-09-02T15:09:30.725Z
tags:
    - pco
title: Creating New Profiles
TocOpen: true
---

[Profiles](https://pcopeople.zendesk.com/hc/en-us/articles/204263114-Profile-Overview)
contain some of our most basic data in Planning Center. Therefore, keeping this
data clean and consistent needs to be a priority for anyone working in the
database.

New profiles are automatically created in the system when someone uses Church
Center to:

- give
- fill out a form
- register for an event

These profiles (and all new profiles) are shown on the **people** dashboard,
which also contains
[a link to a list of all profiles, sorted by creation time](https://people.planningcenteronline.com/people?order=-createdAt).
If you use **people** and happen to see an obvious error in the overview, please
help by correcting it! The tech deacon also keeps
[a workflow](https://people.planningcenteronline.com/workflows/530311/) to check
all new profiles.

New profiles are most frequently *manually* created:

 - when manually entering in giving information on a new donor
 - when checking a visitor into the Children's Ministry
 - and other cases when an admin needs to track a new person

Especially in these manual cases, please be mindful of the data on the new
profile you create. Note that we track non-people (e.g. other churches,
organizations, etc.) [specially](#organizations).

## People

Any time we add a *person* to the database, please ensure that the first and
last names are correctly spelled and capitalized correctly.  Planning Center
provides multiple fields to enter a person's name.  If a person's
legal first name is different from what they are known as, be sure to enter
their "First" and "Given" names, as appropriate.  Please use fields as:

| Field name | Description |
|---|---|
| **First** | The person's preferred name; what they would introduce themselves as. |
| **Given name** | Their legal first name, if different from their **First** name. |
| **Middle** | Their middle name, only if desired. |
| **Last** | Their legal[^1] last name. |
| **Nickname** | The name they are known by, if different from their **First** name.  *This is rare!* |
| **Prefix** | *see below* |
| **Suffix** | *see below* |

### Check-ins

For children, please ensure that the following fields are accurate:

- First and Last names (see [above](#people))
- birthdate
- grade, if any
- medical notes

For parents/guardians, please ensure that the following fields are accurate:

- First and Last names (see [above](#people))
- mobile phone number
- email address, if desired

Please also ensure that the "Household" for these are accurate (see
[below](#households)). In particular, use
[Trusted People](https://pcocheck-ins.zendesk.com/hc/en-us/articles/115005050628-Manage-Household-and-Trusted-People#manage-trusted-people-1)
when a person (e.g. a grandparent not living with the child) should be able to
check-in and checkout a child but is not in the child's household.

### Giving

In addition to the name (see [above](#people)), please ensure we have some sort
of contact information, such as an email, phone number, or mailing address.

### Prefix/Suffix

The **Prefix** and **Suffix** fields provide only limited options (e.g. "Jr." as
a suffix). This is intentional. If you feel a different prefix or suffix is
needed in your case, please contact an admin.

*Please only use the "Dr." prefix for a medical doctor.*

## Organizations

Organizations are typically needed when a donation is received from a group
rather than an individual. In this case, we need at least to track:

- the organization name
  - PCO requires a first *and* last name, so split the organization name in some
    way. For example: **Another city** (first) **Church** (last). Only, ensure
    that when the first and last names are concatenated with a space character,
    the name makes sense (as this is how the organization will be displayed
    throughout PCO).
- the ["Membership
  Type"](https://pcopeople.zendesk.com/hc/en-us/articles/360013192194-Manage-Membership#assign-membership-1)
  must be set to "Corporation".

A contact email, phone, and mailing address will also likely be helpful to us,
if available.

## Households

Households are intended to track a group of people who meet the following
criteria:

- the **Adult** profiles belong to people living in the same household
- the **Child** profiles belong to children under 18 years old (or still in high
  school) living in the same household as the **Adult**s and those adults are
  legally responsible for the **Child**

Households are not to be used for any other situation. (If you feel this is
invalid in your use case, please contact an admin.) A **Child** may belong to
multiple households (e.g. for parents living separately).

If a person should be able to check-in or checkout an individual that is not in
their household, add the person to the child's
[Trusted People](https://pcocheck-ins.zendesk.com/hc/en-us/articles/115005050628-Manage-Household-and-Trusted-People#manage-trusted-people-1).

[^1]: PCO provides only a single option for the last name, and this is used both
in the only directory (for members) and in giving statements. As such, we have
limited options if a member prefers a last name in the directory that is
different from their legal name. In this case, please enter their preferred name
in their profile and, if available for you to edit, their legal name in the
"Legal Last Name" field under the "Misc" tab on their profile.
