---
date: 2024-06-23
tags:
    - pco
title: Matrix View
TocOpen: true
---

I have found the
[Matrix View](https://pcoservices.zendesk.com/hc/en-us/articles/204261764-Setting-Up-the-Matrix#UUID-5147c0dd-ecfe-97a5-69bc-eb09538634d2)
to be ~~very~~ extremely helpful when planning services or scheduling
volunteers. If you plan or schedule frequently, I **strongly** encourage you to
create a
[custom Matrix View](https://pcoservices.zendesk.com/hc/en-us/articles/204261764-Setting-Up-the-Matrix#CreateaCustomMatrix)
to suit your needs. You can select only those parts of the service that are
relevant to your needs (e.g. you can show only your volunteer teams, or hide the
order of service).

Some thoughts:
- you can click and drag items and people to other services or positions
  - people are always copied
  - items are copied if to a different service, else moved if in the same service
- you can use
  [Target View](https://pcoservices.zendesk.com/hc/en-us/articles/9133975623963-New-See-every-instance-of-people-or-items-in-the-Matrix-with-Target-View)
  to highlight items or people wherever they occur in all the plans shown
