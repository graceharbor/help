---
title: PCO Migration First Steps
date: 2024-06-23
weight: 99
showToc: false
---

As mentioned at the members' meeting, our church's data
[is now in Planning Center](./why-transition).  This tool will help reduce
administrative overhead and help us be better organized.

However, I need your help. The system will not serve us well if our data
(you and your childrens' info) [is no good](https://xkcd.com/2295/).
Additionally, our new online member directory will be managed in Planning
Center; while I can *invite* you to join the directory, I cannot actually *place
you* in it.

What do you need to do? Please complete the steps below. These steps should take
you no longer than 10 minutes. If you run into problems, it is taking you too
long, or you feel like you need help getting started, consider
[signing up for a time slot](https://graceharborpvd.churchcenter.com/registrations/events/2369449)
and I can help you in-person.

*Note: if you have a spouse who is also a member, the following steps may not
fully work using the Church Center phone app; you may need to go to the
following links in a web browser instead.*

## Steps to be completed

1. Login to [Church Center](https://graceharborpvd.churchcenter.com/me) (need [_help_](../members)?)

2. Complete [the data migration form](https://graceharborpvd.churchcenter.com/people/forms/746039)
   in order to ensure that our data is accurate.

3. You should have also received an introductory email (example below).  Click on the __Join Directory__
   button in the email.  If you have completed Steps 1 and 2, you should be able to see
   the directory, and you should be in it!  

![PCO directory invitation email](/image/pco-directory-invite.png "Example Planning Center directory invitation email")
