---
title: Using PCO as a Member
summary: How to use Planning Center as a GHC member.
date: 2024-06-23
weight: 10
---

*Note: a condensed form of these instructions are also available as
[a PDF](https://drive.google.com/file/d/1BCQVds3GAln6Xc7sTDlyZHCiyBiLAgA9/view?usp=sharing).*

# How to

## ...login to Church Center

In your role as a member, your primary access to Planning Center data is either through the web portal at:

<span style="width: 100%; text-align: center;">

https://graceharborpvd.churchcenter.com

</span>

or through the “Church Center App” (see
[the help guide](https://pcochurchcenter.zendesk.com/hc/en-us/articles/6317711061275-Use-Church-Center))
from one of the stores:

<div style="display: flex; align-items: center; width: 90%; padding-top: 10px; padding-bottom: 20px;">
    <div style="flex: 1; text-align: center;">
        <!-- SVG Image for dark mode -->
        <a href="https://apps.apple.com/us/app/church-center-app/id1357742931">
        <picture style="display: flex; justify-content: center;">
            <source srcset="/image/apple_store_dark.svg" media="(prefers-color-scheme: dark)">
            <img style="height: 50px;" src="/image/apple_store_light.svg" alt="Apple Store Logo">
        </picture>
        </a>
    </div>
    <div style="flex: 1; text-align: center;">
        <a href="https://play.google.com/store/apps/details?id=com.ministrycentered.churchcenter&hl=en_US">
        <!-- PNG Image for Play Store -->
        <picture style="display: flex; justify-content: center;">
            <img style="height: 50px;" src="/image/play_store.png" alt="Google Play Store Logo">
        </picture>
        </a>
    </div>
</div>


WIth any of those options, log in using either your phone number or email
address, as it appears in the printed directory. Planning Center will send a
code to your phone or email, which allows you to access the system.

## ...login to Planning Center

You may also be given access to one or more of the Planning Center web modules
(with a `planningcenteronline.com` domain link instead of `churchcenter.com`).
The process is the same as above, except you will be asked to create a password
on your first login, which you will use for future visits.

If you need to reset your password, that is done
[through Planning Center's website](https://login.planningcenteronline.com/password_reset/new).

## ...update your (and your family members') profile.

Updating your profile (your photo, phone number, email, address, etc.) is done
through Church Center, either the web or phone app. You may also update the
information for others in your household. For additional information, please see
the Planning Center help articles:

- [Updating your profile][pco-update-profile]
- [Updating your family member's profile][pco-update-household]

Go to https://graceharborpvd.churchcenter.com/me, or click on your profile picture
in the upper left corner if you are already at Church Center.  Under your name,
click on [__My profile & settings__](https://graceharborpvd.churchcenter.com/profile).

### Update your own profile

On your settings page, click
![Contact & profile information](/image/pco/contact_and_profile.png) to view
your contact and profile information. On that page, you may edit your
information by clicking on one of the edit buttons: ![](/image/pco/edit.png). To
change your profile picture, click on the pencil icon over your current picture:
![](/image/pco/pencil.png).

If you need to update your name or birthdate, you will need to contact
admin@graceharbor.net or tech@graceharbor.net. If you prefer to go by a
different name, let us know (for example, I prefer "Tim" to "Timothy", so in my
Planning Center data, I have "Timothy" stored as my [given name][pco-names], but
"Tim" as my [first name][pco-names]).

You may enter multiple emails, phone numbers, or addresses, but please keep in
mind that the following are shown in the printed directory:

- your name, as displayed on this page
- your photo (please use a clear head shot with a plain background)
- the phone number you have marked as "Primary"
- the email address you have marked as "Primary"
- the street address you have marked as "Primary"

### Update your family member's profile

On your settings page, click ![Household](/image/pco/household.png) to view the
people currently a part of your household. Your household should consist of you
and, if applicable, your children who are high school seniors or younger.

From this page, you may edit the information on members of your household by
clicking on their photo/name. Keep in mind that their name, as shown on this
page, is how it will appear in the directory. If you need to modify their name
or birthdate, you will need to contact us.

Although it is possible to add a new family member from this page, I would prefer
if you use [this form][form-add-child] or contact me instead.

## ...access the church directory

The church directory has moved from CTRN to Planning Center. You will now access
the directory through the Church Center App on your phone, or through the web
at:

<span style="width: 100%; text-align: center;">

https://graceharborpvd.churchcenter.com/directory

</span>

As a convenience, the following URL redirects to Church Center:

<span style="width: 100%; text-align: center;">

[directory.graceharbor.net](https://directory.graceharbor.net)

</span>

[pco-names]: https://pcopeople.zendesk.com/hc/en-us/articles/360008652773-First-Name-Given-Name-and-Nicknames?wtime=105s&wchannelid=7ba4ph91tm&wmediaid=jivzt0xn5g
[pco-update-profile]: https://pcochurchcenter.zendesk.com/hc/en-us/articles/7992221192347-Update-Your-Profile
[pco-update-household]: https://pcochurchcenter.zendesk.com/hc/en-us/articles/22668444926875-Manage-Household-Members
[form-add-child]: https://graceharborpvd.churchcenter.com/people/forms/759936
