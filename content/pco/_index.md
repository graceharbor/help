---
title: GHC on Planning Center
date: 2024-06-23
cover:
  image: image/ghc_to_pco.svg
---

As part of our church's transition to using Planning Center for our data
management, these guides provide information for how you can best use
the new system.

Note that the following terms may be used interchangeably:
 - Planning Center
 - Planning Center Online
 - PCO
  
Additionally, Planning Center provides an online portal which they call "Church
Center". On the portal, you can access and edit your own profile data, manage
your online giving and view giving history, and view the member directory,

Browse or [search](/search) these help pages, or view
[the introductory presentation](./presentation.html).
