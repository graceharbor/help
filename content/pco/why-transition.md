---
date: 2024-06-23
tags:
    - pco
title: Why move to Planning Center?
TocOpen: true
weight: 1
---

Our church administrative data was spread across many different applications,
with different people using different system to help support their role as admin
or deacon or volunteer. These systems did not integrate with one another, which
made for a tedious and error-prone process whenever people's contact info
changed, and as people moved into and out of membership in our church.

Volunteer scheduling was being done in three different applications, with
volunteers and schedulers needing to check different systems depending on their
role, each requiring seperate login information.  Again, contact information
was spread between applications with updates happening ~~rarely~~ never.

I researched about 10 different applications built specifically for helping
churches manage administrative data with a range of features and prices. After a
few months of gathering information on these different applications and
consulting with others to determine our needs, I spent an additional few months
testing two of these systems, with the help and input from several other people
in the church.

Because a large part of our church was already using
[Planning Center](https://www.planningcenter.com/about) for tracking donations,
planning the Childrens' Ministry volunteer schedule, and managing the kids
check-in process on Sunday mornings, we felt that expanding our use of Planning
Center would be the easiest move for the time being.

Using Planning Center will be a great help to several of us, especially our
administrator. The biggest change for most members will be that the church
directory is no longer in CTRN, but in Church Center.

To learn how this change will impact you, browse the articles under
[the main PCO transition page](..), or [search the help site](/search).
