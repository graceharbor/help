---
title: GHC @ PCO
summary: Introductory presentation given at 2024-06-23 members' meeting.
type: slides
hiddenInList: true
url: /pco/presentation.html
reveal:
  theme: white
  plugins:
    - markdown
    - notes
  config: |
    {
      "controlsLayout": "edges",
      "transition": "none",
      "progress": true,
      "hash": true
    }
  init:
    center: false
---

<style type="text/css">
  .reveal section p {
    text-align: left;
  }
  .reveal section ul {
    display: block;
  }
  .reveal section ol {
    display: block;
  }
  .large {
    font-size: 30pt;
  }
  .church_center img {
    width: 80%;
  }
  .church_center figure,
  .church_center img {
    margin: 0 auto;
    width: 550px;
  }
  ul ul {
    font-size: 26pt;
  }
</style>


<section class="center">
We are moving to Planning Center!

![GHC logo @ PCO logo](/image/ghc_to_pco.svg)

<aside class="notes">

- __what__ are we doing and __why__
- __what__ you need to do and __why__
- __when__ and __how__ you need to do it

</aside>

</section>

<section data-markdown><textarea data-template>

## Wait.  What?  Why?

- What?
  - Consolidating data into a new system from: <!-- .element: class="smaller" -->
    - CTRN online directory
    - Mailchimp
    - swerv
    - Google Groups
    - Spreadsheets
- Why?<!-- .element: class="fragment" data-fragment-index="2" -->
  - Reduce workload
  - Reduce errors
  - Ease communication

<aside class="notes">

- swerv is for service planning

- *lots of manual data synchronization tasks*
- *Why wasn't I on that email?*
- *Why isn't my child's name on the Sunday School roster?*
- *Who is leading this week?*

</aside>
</textarea></section>
<section data-markdown><textarea data-template>

## Wait.  What?  Why?

- What?
  - Consolidating data into a new system from: <!-- .element: class="smaller" -->
    - ~~CTRN online directory~~
    - Mailchimp
    - ~~swerv~~
    - Google Groups
    - Spreadsheets
- Why? 
  - Reduce workload
  - Reduce errors
  - Ease communication

<aside class="notes">

- CTRN ending next members meeting
- swerv ending in July

</aside>

</textarea></section>

<section data-markdown><textarea data-template>

## Planning Center

**Planning Center**:
</br>
a web-based tool to manage our data and processes

 - It also goes by...
   - Planning Center Online
   - PCO

<aside class="notes">

- 3 months looking at 10+ apps
- 3 months in-depth with 2

</aside>

</textarea></section>

<section data-markdown><textarea data-template>

<div class="church_center" >

![Church Center App logo](/image/pco/church_center.svg)

</div>

A portal to Planning Center data:

- update profile picture & contact info
- view the [*__member directory__*](https://directory.graceharbor.net)
- give online (& view past giving)
- as a volunteer,
  - view your schedule
  - respond to requests
  - set upcoming availability
- sign-up for events
- ...more in the future
  
<div style="position: relative; left: 300px; top: -300px;" class="fragment" data-fragment-index="2">
  <a href="https://apps.apple.com/us/app/church-center-app/id1357742931">
  <img style="height: 60px;" src="/image/apple_store_light.svg" alt="Apple Store Logo">
  </a>
  <br/>
  <a href="https://play.google.com/store/apps/details?id=com.ministrycentered.churchcenter&hl=en_US">
  <img style="height: 60px;" src="/image/play_store.png" alt="Google Play Store Logo">
  </a>
</div>

<aside class="notes">

- web or iOS or android

</aside>

</textarea></section>


<section data-markdown><textarea data-template>

## What you need to do

_You will receive two emails with instructions:_

- Login to [Church Center](https://graceharborpvd.churchcenter.com/me)
  - passwordless; just need phone or email
- Fill out [this online form](https://graceharborpvd.churchcenter.com/people/forms/746039)
- Join the [directory](https://directory.graceharbor.net)

</textarea></section>

<section data-markdown><textarea data-template>

## Why should you?

- I cannot actually place you in the directory
  - I can only invite you; you need to accept
- I need your help verifying your info <!-- .element: class="fragment" data-fragment-index="2" -->
- should take no more than 10 minutes <!-- .element: class="fragment" data-fragment-index="3" -->
- I'll provide <!-- .element: class="fragment" data-fragment-index="4" -->
  [help sessions](https://graceharborpvd.churchcenter.com/registrations/events/2369449) <!-- .element: class="fragment" data-fragment-index="4" -->
  - next two Sundays, after our morning gathering
  - next two Sundays, before our evening gathering

<aside class="notes">

- more dates added if needed

</aside>

</textarea></section>


<section data-background-color="black"></section>
