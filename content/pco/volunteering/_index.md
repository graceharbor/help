---
title: Using PCO as a Volunteer
summary: How to use Planning Center as a volunteer.
date: 2024-06-23
weight: 11
---

In addition to the articles mentioned in this section, Planning Center provides
[a list of articles for the basics of being a team member](https://pcoservices.zendesk.com/hc/en-us/sections/200765784-Team-Members-Basics).
Once your scheduler has added you to the schedule, you will receive an email
indicating the dates that you are to serve. You will help your scheduler by
responding to that email (you may do so directly from the buttons provided in
the email; you do not need to log in).

## Viewing the volunteer schedule

Your personal schedule is available on Church Center:

<span style="width: 100%; text-align: center;">

https://graceharborpvd.churchcenter.com/me

</span>

As a volunteer, you can view the full volunteer schedule at:

<span style="width: 100%; text-align: center;">

https://services.planningcenteronline.com

</span>

*Note: if you can only view the weeks you are serving but not others, speak with
your scheduler about making you a "Scheduled Viewer".*

You may also find [the Church Center App](../members) to be useful. Planning
Center also offers a phone app specific to the services module, available on [iOS](https://apps.apple.com/us/app/planning-center-services/id327370808) and 
[Android](https://play.google.com/store/apps/details?id=com.ministrycentered.PlanningCenter&hl=en_US).

## Synching your personal schedule with your calendar app

You can add an `iCal` of your serving schedule to your calendar app (e.g. Google
Calendar). See
[the Planning Center article](https://pcoservices.zendesk.com/hc/en-us/articles/360002036253-Calendar-Subscriptions)
for how to subscribe.

## Updating your serving preferences

If you prefer to serve a limited number of times per month, you may [set your
scheduling preferences](https://pcoservices.zendesk.com/hc/en-us/articles/360002036233-Set-Your-Scheduling-Preferences).

## Updating your availability

If you have a block of dates that you cannot serve, please update your
availability in Church Center so that your scheduler can see this information
when creating the volunteer schedule.

Go to your profile page (https://graceharborpvd.churchcenter.com/me, or in the
phone app), and under __Schedule__, click the __Actions__ button and "Add
blockout". Your blocked out dates are shown in your schedule and viewable by
your scheduler.

Alternatively, you can [add blockout dates](https://pcoservices.zendesk.com/hc/en-us/articles/360002035974-Blocking-Out-Dates) in the [__services__ module](https://services.planningcenteronline.com/dashboard/0) at:

<span style="width: 100%; text-align: center;">

https://services.planningcenteronline.com

</span>

## Access lyrics and chord sheets

If you serve on the music or tech teams, you also have access to lyrics and
chord sheets, as you did in `swerv`. Please click on the “Songs” link at the
header any page in the
[__services__ module](https://services.planningcenteronline.com/dashboard/0).
Alternatively, for a specific plan, view the plan in the
[__services__ module](https://services.planningcenteronline.com/dashboard/0)
(linked from your schedule in Church Center) and click on the __Rehearse__ tab
to view the songs for that given week, including a link to download a single PDF
of all songs and the order of service.
