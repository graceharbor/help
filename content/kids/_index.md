---
title: GHC Children's Ministry
date: 2024-08-29
---

*This site contains some help pages for the GHC Children's Ministry. For
information about the ministry, please
[see the main GHC website](https://www.graceharbor.net/kids).*
