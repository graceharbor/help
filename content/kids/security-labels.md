---
title: Purpose of Security Labels
date: 2024-08-29
---

*Find more information on our Children's Ministry on our
[main website](https://www.graceharbor.net/kids).*

When you check your child into the Children's Ministry, you will receive
a security label with a four-character code, while your child will
receive a name tag with the matching code.  Your security label will look
like this:

![Security Label](/image/kids-why.png "Example security label for check-in")

This may feel like your child has become a number in a system, but the contents
of this label serve an important purpose.

## Label contents

### The time of today's gathering

The top of the label lists the date and time of our gathering , which marks the
only gathering for which this label is valid. A child cannot be picked up using
an invalid label.

### The security code

The main section of the label features a four-character security code. This code
is randomly generated when your child is checked-in, and is only valid for the
current gathering (it will change each week). If you have multiple children
checked-in, the security code for all children is identical, and you only
receive one security label.

Present this label to the classroom teacher or helper when you pick up your
child and identify your child by name. The teacher/helper will match the code
with your child to ensure that your child may only be released to you. In this
way, the security label is a sort of identification to match you and your child.

### A link to this page

Finally, the label reminds you to retain it in order to pickup your child, and
has an URL that lands on this page.

### No identifying information

Note that there is no other identifying information on the label. This means
that if you misplace the label, anyone that finds that label will not know which
child has the matching security code, and therefore cannot pick up your child.
*For this reason, we ask that you identify your child by name when you pick them
up.*

## What if I misplaced my label?

If you have misplaced your label, please have identification ready when you pick
up your child so that the classroom teacher/helper can verify that your child
may be released to you.
